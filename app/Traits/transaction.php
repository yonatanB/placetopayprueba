<?php

namespace App\Traits;

use App\Persona;
use App\Transaction_Model;
use App\Info_Transaction;

trait transaction
{
    public function PSETransactionRequest($request) 
    {
            $pserequest=array(
             "bankCode" =>$request['idbanks'],
             "bankInterface" =>$request['idtipo'],
             "returnURL" =>'http://localhost:8000/receive',
             "reference" => '190.85.82.130.CC.123456789',
             "description" => 'Compra a EGM - Pruebas soporte - 73',
             "language" => '',
             "currency" => 'COP',
             "totalAmount" => 1000.00,
             "taxAmount" => 0.00,
             "devolutionBase" => 0.00,
             "tipAmount" => 0.00,
             "payer"=>$this->persona($request),
             "buyer"=>$this->persona($request),
             "shipping"=>$this->persona($request),
             "ipAddress" =>$this->getRealIP(),
             "userAgent" => '',
             "additionalData" => array()
            );

            //$transaction= array('transaction'=> $pserequest);

            return $pserequest;


    }

    public function persona($request) 
    {

            $person= array(
                'documentType'=> $request['iddocumento'],
                'document'=> $request['doc'],
                'firstName'=> $request['nombre'],
                'lastName'=> $request['apellido'],
                'company'=> $request['empresa'],
                'emailAddress'=> $request['correo'],
                'address'=> $request['direccion'],
                'city'=> $request['idciudad'],
                'province'=> $request['iddepartamento'],
                'country'=> 'colombia',
                'phone'=> $request['telefono'],
                'mobile'=> $request['telefono']
            );
             
            //$person= array($nombre => $person);

            return $person;

    }

    public function getRealIP() 
    {
            if (!empty($_SERVER['HTTP_CLIENT_IP']))
            return $_SERVER['HTTP_CLIENT_IP'];

            if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            return $_SERVER['HTTP_X_FORWARDED_FOR'];

            return $_SERVER['REMOTE_ADDR'];
    }

    public function SavePersona ($response,$transactionID)
    {

        $user=new Persona;
        $user->tipo_documento=$response['documentType'];
        $user->documento=$response['document'];
        $user->nombres=$response['firstName'];
        $user->apellidos=$response['lastName'];
        $user->empresa=$response['company'];
        $user->email=$response['emailAddress'];
        $user->direccion=$response['address'];
        $user->id_ciudad=$response['city'];
        $user->id_departamento=$response['province'];
        $user->telefono=$response['phone'];
        $user->celular=$response['mobile'];
        $user->transactionID=$transactionID;
        $user->save();

    }

    public function SaveTransaction ($response)
    {

        $tran=new Transaction_Model;
        $tran->transactionID=$response->transactionID;
        $tran->sessionID=$response->sessionID;
        $tran->returnCode=$response->returnCode;
        $tran->trazabilityCode=$response->trazabilityCode;
        $tran->transactionCycle=$response->transactionCycle;
        $tran->bankCurrency=$response->bankCurrency;
        $tran->bankFactor=$response->bankFactor;
        $tran->bankURL=$response->bankURL;
        $tran->responseCode=$response->responseCode;
        $tran->responseReasonCode=$response->responseReasonCode;
        $tran->responseReasonText=$response->responseReasonText;
        $tran->save();
    }

    public function Save_Info_Transaction ($response)
    {
        $info=new Info_Transaction;
        $info->transactionID=$response->transactionID;
        $info->sessionID=$response->sessionID;
        $info->reference=$response->reference;
        $info->requestDate=$response->requestDate;
        $info->bankProcessDate=$response->bankProcessDate;
        $info->onTest=$response->onTest;
        $info->returnCode=$response->returnCode;
        $info->trazabilityCode=$response->trazabilityCode;
        $info->transactionCycle=$response->transactionCycle;
        $info->transactionState=$response->transactionState;
        $info->responseCode=$response->responseCode;
        $info->responseReasonCode=$response->responseReasonCode;
        $info->responseReasonText=$response->responseReasonText;
        $info->save();
    }

}