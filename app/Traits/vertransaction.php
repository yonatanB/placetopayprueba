<?php

namespace App\Traits;
use DB;

trait vertransaction
{
   public function GetTransactions()
   {

   	 $tran=DB::table('personas as a')
   	   ->select('a.*','b.transactionID as ID','b.requestDate','b.bankProcessDate','b.transactionState','b.responseReasonText')
       ->join('informacion_transaccion as b','a.transactionID','b.transactionID')
       ->get();

       return $tran;
   }

}