<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction_Model extends Model
{
   protected $table='respuesta_transaccion';

    protected $primaryKey='id';

    public $timestamps=false;


    protected $fillable =[ // campos que recibiran valores y se almacenaran en la base de datos

  'transactionID',
  'sessionID',
  'returnCode',
  'trazabilityCode',
  'transactionCycle',
  'bankCurrency',
  'bankFactor',
  'bankURL',
  'responseCode',
  'responseReasonCode',
  'responseReasonText'

    ];

    protected $guarded =[

    ];
}
