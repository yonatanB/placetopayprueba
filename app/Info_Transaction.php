<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Info_Transaction extends Model
{
     protected $table='informacion_transaccion';

    protected $primaryKey='id_info';

    public $timestamps=false;


    protected $fillable =[ // campos que recibiran valores y se almacenaran en la base de datos

  'transactionID',
  'sessionID',
  'reference',
  'requestDate',
  'bankProcessDate',
  'onTest',
  'returnCode',
  'trazabilityCode',
  'transactionCycle',
  'transactionState',
  'responseCode',
  'responseReasonCode',
  'responseReasonText'

    ];

    protected $guarded =[

    ];
}
