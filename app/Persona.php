<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $table='personas';

    protected $primaryKey='id_person';

    public $timestamps=false;


    protected $fillable =[ // campos que recibiran valores y se almacenaran en la base de datos

  'tipo_documento',
  'documento',
  'nombres',
  'apellidos',
  'empresa',
  'email',
  'direccion',
  'id_ciudad',
  'id_departamento',
  'telefono',
  'celular',
  'transactionID'

    ];

    protected $guarded =[

    ];
}
