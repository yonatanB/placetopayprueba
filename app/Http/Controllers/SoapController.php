<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use SoapClient;
use App\Traits\Authenticationserve;
use App\Traits\transaction;
use App\Persona;
use App\Transaction_Model;
use App\Info_Transaction;
use DB;
use Session;

class SoapController extends BaseSoapController
{
    use Authenticationserve,transaction;

    private $service;

    public function __construct()
    {
        self::setWsdl('https://test.placetopay.com/soap/pse/?wsdl');
        $this->service = InstanceSoapClient::init();

        $this->service->__setLocation('https://test.placetopay.com/soap/pse/');
    }

    public function index(){
        
        $departamentos=DB::table('tbl_departamentos')
            ->select('departamento_id','departamento_nombre as departamento')
            ->where('departamento_pais_id','=',1)
            ->orderby('departamento_nombre')
            ->get();
        return view('pago.inicio',['departamentos'=>$departamentos]);
    }

    public function GetBanksList(Request $request)
    {
       try {
            
                if(Cache::get('banklist'))
                {
                    $bancos=Cache::get('banklist');
                }
                else
                {
                    $auth= array('auth'=> $this->auth());
                
                    $response = $this->service->__soapCall("getBankList",array($auth));

                    if(sizeof($response)>0)
                      {
                        $bancos=json_encode($response->getBankListResult->item);
                        $expiresAt = now()->addMinutes(86400);
                        Cache::put('banklist',$bancos, $expiresAt);
                      }

                 }
                
                return $bancos;
            
        }
        catch(\Exception $e) {
            return $e->getMessage();
        }
    }

    public function CreateTransaction(Request $request)
    {

      $info_tran=$request->all();

      try {
   
           $parameters= array('auth'=> $this->auth(),'transaction'=>$this->PSETransactionRequest($info_tran));

           $response = $this->service->__soapCall('createTransaction',array($parameters));

                if($response->createTransactionResult->returnCode=='SUCCESS')
                {
                    $this->SavePersona($this->persona($info_tran),$response->createTransactionResult->transactionID);

                    $this->SaveTransaction($response->createTransactionResult);

                   Session::put('transactionID',$response->createTransactionResult->transactionID);
                    
                    return $response->createTransactionResult->bankURL;
                }else
                {
                    return $response->createTransactionResult->responseReasonText;
                }
                       
        }
        catch(\Exception $e) {
            return $e->getMessage();
        }
        
    }

    public function getTransactionInformation()
    {
        try {

               $parameters= array('auth'=> $this->auth(),'transactionID'=>Session::get('transactionID'));

               $response = $this->service->__soapCall('getTransactionInformation',array($parameters));

                 $this->Save_Info_Transaction($response->getTransactionInformationResult);
               
               return view('pago.banco',['response'=>$response->getTransactionInformationResult->responseReasonText,'state'=>$response->getTransactionInformationResult->transactionState]);
               $request->session()->flush();
               

            }
        catch(\Exception $e) {
            return $e->getMessage();
        }
    }

}
