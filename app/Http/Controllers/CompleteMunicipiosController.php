<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CompleteMunicipiosController extends Controller
{
    public function get_municipios(Request $request)
    {
         if ($request)
        {
        	$departamento=$request->depto;
            
            $municipios=DB::table('tbl_municipios')
            ->select('municipio_id','municipio_nombre as municipio')
            ->join('tbl_departamentos','tbl_municipios.municipio_departamento_id','tbl_departamentos.departamento_id')
            ->where('departamento_nombre','=',$departamento)
            ->orderby('municipio_nombre')
            ->get();

            return json_encode($municipios); 

        }
    }
}
