<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Traits\vertransaction;

class VerTransacciones extends Controller
{
	use vertransaction;

    public function ListarTransacciones(){
       
       $trans=$this->GetTransactions();
       return view('pago.listar',['transactions'=>$trans]); 
    }
}
