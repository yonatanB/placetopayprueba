<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/', 'SoapController@index');
Route::get('/banks', 'SoapController@GetBanksList');
Route::post('/create', 'SoapController@CreateTransaction');
Route::get('/receive', 'SoapController@getTransactionInformation');
Route::get('/listar', 'VerTransacciones@ListarTransacciones');
Route::get('/autocomplete/municipios
', 'CompleteMunicipiosController@get_municipios');

