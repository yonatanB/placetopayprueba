$(document).ready(function(e){
 jQuery('#selectdep').on('change',function(e){
        e.preventDefault();
        var data = {
          depto: jQuery('#selectdep').val()
        }
      jQuery.ajax({
        url:'/autocomplete/municipios',
        data: data,
        success: function(data){
          data = JSON.parse(data);
          jQuery('#selectciu option').remove();
          jQuery('#selectciu').append(new Option('Seleccione', ''));
          jQuery.each(data, function( index, element ) {
            jQuery('#selectciu').append(new Option(element.municipio, element.municipio));
          });
        
        }
      });     
    });
});