$(document).ready(function(e){
   $('#getbanks').click(function() {
       document.getElementById("listbanks").style.display = 'block'

        $.ajax({
          url: '/banks',
          //data: data,
          success: function(data){
            data = JSON.parse(data);
            //jQuery('#selectbank option').remove();
            jQuery.each(data, function( index, element ) {
              jQuery('#selectbank').append(new Option(element.bankName, element.bankCode));
            });
          
          },
          error: function(xhr, status, error) {
                  alert('No se pudo obtener la lista de Entidades Financieras, por favor intente más tarde')
                }
        });
      
   });  

});