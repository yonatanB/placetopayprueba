<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Prueba PlacetoPay</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
    <!-- Theme style -->
    <!--<link rel="stylesheet" href="{{asset('css/AdminLTE.min.css')}}">-->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset('css/_all-skins.min.css')}}">

    <link rel="apple-touch-icon" href="{{asset('img/logo.png')}}">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js" type="text/javascript"></script>

  </head>
  <body class="hold-transition skin-blue sidebar-mini">
   
<div class="panel-body">
      <header class="page-header">

        <section class="content">
              <div class="row">
                <div class="col-md-12">
                  <div class="box">
                    <div class="box-body col-md-12">
                       <h3>PlacetoPay</h3>
                    </div>
                  </div>
                </div>
              </div>
          </section>

      </header>
   
    <!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      
        
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <!-- /.box-header -->
                <div class="box-body col-md-12">
                    <div class="row">
                          <div class="col-md-12">
                              <!--Contenido-->
                              @yield('contenido')
                              <!--Fin Contenido-->
                           </div>
                    </div>
                        
                      </div>
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
        </section> <!--/.content -->
      
      <!--Fin-Contenido-->
      <footer class="footer">

      </footer>

</div><!-- /.content-wrapper -->
    <!-- jQuery 2.1.4 -->
    <!--<script src="{{asset('js/jquery-2.1.4.min.js')}}"></script>-->
    <!-- jQuery 3.2.1 -->
    <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
     <!-- jQuery ui -->
    <script src="{{asset('js/jquery-ui.js')}}"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('js/app.min.js')}}"></script>
    <!--Contenido javascript-->
    @yield('script')
    <!--Fin Contenido javascritp-->
    
  </body>
</html>

