
	<div class="form-horizontal col-lg-8 col-md-8 col-sm-8 col-xs-12 col-lg-offset-1" >
			<div class="form-group">
	            <label class="control-label col-sm-4" for="iddocumento">Tipo de Documento*</label>
	            <div class="col-sm-8">
	          	  <select name="iddocumento" id="tipodoc" class="form-control" required>
	                <option value="">-</option>
	                <option value="CC">Cédula de ciudanía colombiana</option>
	                <option value="CE">Cédula de extranjería</option>
	                <option value="TI">Tarjeta de identidad</option>
	                <option value="PPN">Pasaporte</option>
	                <option value="NIT">Número de identificación tributaria</option>
	                <option value="SSN">Social Security Number</option>
	              </select>
	            </div>
	        </div>
	        <div class="form-group">
	          	<label class="control-label col-sm-4" for="doc">Documento*</label>
	             <div class="col-sm-8">
	          	<input type="text" name="doc" id="docs" class="form-control" required>
	             </div>
	        </div>
	        <div class="form-group">
	          	<label class="control-label col-sm-4" for="nombre">Nombres*</label>
	             <div class="col-sm-8">
	          	<input type="text" name="nombre" id="nombres" class="form-control" required>
	             </div>
	        </div>
	        <div class="form-group">
	          	<label class="control-label col-sm-4" for="apellido">Apellidos*</label>
	             <div class="col-sm-8">
	          	<input type="text" name="apellido" id="apellidos" class="form-control" required>
	             </div>
	        </div>
	        <div class="form-group">
	          	<label class="control-label col-sm-4" for="empresa">Empresa*</label>
	             <div class="col-sm-8">
	          	<input type="text" name="empresa" id="empresas" class="form-control" required>
	             </div>
	        </div>
	        <div class="form-group">
	          	<label class="control-label col-sm-4" for="correo">Correo Electronico*</label>
	             <div class="col-sm-8">
	          	<input type="email" name="correo" id="correos" class="form-control" required>
	             </div>
	        </div>
	        <div class="form-group">
	          	<label class="control-label col-sm-4" for="direccion">Dirección*</label>
	             <div class="col-sm-8">
	          	<input type="text" name="direccion" id="direccions" class="form-control" required>
	             </div>
	        </div>
	        <div class="form-group">
	            <label class="control-label col-sm-4" for="iddepartamento">Departamento*</label>
		        <div class="col-sm-8">
		      	  <select name="iddepartamento" id="selectdep" class="form-control" required>
		            <option value="">-</option>
		            @foreach ($departamentos as $dep)
		            <option value="{{$dep->departamento}}">{{$dep->departamento}}</option>
		            @endforeach
		          </select>
		        </div>
	        </div>
	        <div class="form-group">
	            <label class="control-label col-sm-4" for="idciudad">Ciudad*</label>
	            <div class="col-sm-8">
	                <select name="idciudad" id="selectciu" class="form-control" required>
	                <option value="">-</option>
	                </select>
	            </div>
	        </div>
	        <div class="form-group">
	          	<label class="control-label col-sm-4" for="telefono">Teléfono*</label>
	            <div class="col-sm-8">
	          	<input type="text" name="telefono" id="tel" class="form-control" placeholder="Teléfono" required>
	            </div>
	        </div>
	</div>


