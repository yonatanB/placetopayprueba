@extends ('layouts.admin')
@section ('contenido')
<div class="panel panel-default">
   <div class="panel-body">
   	    <div class="row">
	   	    <div class="col-md-8 col-md-offset-2">
	   	    	<h4>{{$response}}</h4>
	   	    	<input id="estado" type="hidden" value="{{$state}}">
	   	    </div>
	   	</div>
	</div>
</div>
<script src="{{asset('js/revisartransaction.js')}}"></script>
@endsection