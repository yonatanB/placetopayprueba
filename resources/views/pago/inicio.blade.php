@extends ('layouts.admin')
@section ('contenido')
<div class="panel panel-default">
   <div class="panel-body">
   	    <div class="row">
   	    	<div class="col-md-4 pull-right">
   	    		<a class="btn btn-primary pull-right" href="/listar">Ver Transacciones</a>
   	    	</div>
   	    </div>
   	    <div class="row">
   	    	<br><br>
   	        <form id="" action="" name="" method="post" enctype="multipart/form-data" class="form-vertical payer" >
        	<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="col-md-8">
				   @include('pago.formuser')
				</div>
				<div class="col-md-4">
			        <a id="getbanks" class="col-md-8 col-md-offset-3" href="#"><img id="pse" height="80" src="{{asset('img/pse.jpg')}}"></a>
			        <br>
			        <div class="col-sm-12" id="listbanks" style="display: none;">
				      <div class="form-group">
			            <label class="control-label col-sm-2" for="idbanks">Banco(*)</label>
			            <div class="col-sm-8">
			               <select name="idbanks" id="selectbank" class="form-control" required>
			               	<option value="">-</option>
			               </select>
			            </div>
			          </div>
			          <br><br>
			          <div class="form-group">
			            <label class="control-label col-sm-2" for="idbanks">Tipo(*)</label>
			            <div class="col-sm-8">
			               <select name="idtipo" id="selecttipo" class="form-control" required>
			               	<option value="">-</option>
			               	<option value="0">PERSONAS</option>
			               	<option value="1">EMPRESAS</option>
			               </select>
			            </div>
			            <div class="col-sm-8 col-md-offset-4">
			            <br><br>
	                      	  <button class="btn btn-primary" type="submit">Confirmar</button>
	                    </div>
			          </div>
				    </div>
			    </div>
			</form>
	    </div>
	</div>

</div>
 @section ('script')
  <script src="{{asset('js/getbanks.js')}}"></script>
 @endsection
 <script src="{{asset('js/municipios.js')}}"></script>
 <script src="{{asset('js/createtransaction.js')}}"></script>
@endsection

