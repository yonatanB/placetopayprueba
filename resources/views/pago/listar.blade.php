@extends ('layouts.admin')
@section ('contenido')
<div class="panel panel-default">
   <div class="panel-body">
   	 <div class="row">
   	 	<div class="col-md-12">
   	 	  <table class="table table-striped" >
            <thead>
              <tr>
              	<th style="text-align: center;">Transaccion ID</th>
                <th style="text-align: center;">Nombres</th>
                <th style="text-align: center;">Apellidos</th>
                <th style="text-align: center;">Documento</th>
                <th style="text-align: center;">Empresa</th>
                <th style="text-align: center;">Estado Transaccion</th>
                <th style="text-align: center;">Mensaje</th>
                <th style="text-align: center;">Fecha Solicitud</th>
                <th style="text-align: center;">Fecha Proceso Banco</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($transactions as $key=>$tra)
              <tr>
                <td style="vertical-align:middle; text-align: center;">{{$tra->ID}}</td>
                <td style="vertical-align:middle; text-align: center;">{{$tra->nombres}}</td>
                <td style="vertical-align:middle; text-align: center;">{{$tra->apellidos}}
                </td>
                <td style="vertical-align:middle; text-align: center;">{{$tra->documento}}</td>
                <td style="vertical-align:middle; text-align: center;">{{$tra->empresa}}</td>
                <td style="vertical-align:middle; text-align: center;">{{$tra->transactionState}}</td>
                <td style="vertical-align:middle; text-align: center;">{{$tra->responseReasonText}}</td>
                <td style="vertical-align:middle; text-align: center;">{{$tra->requestDate}}</td>
                <td style="vertical-align:middle; text-align: center;">{{$tra->bankProcessDate}}</td>
              </tr>
              @endforeach
           </tbody>
          </table>
   	 	</div>
   	 </div>
   </div>
</div>
@endsection